import NetworkRequestInfo from "./NetworkRequestInfo";
import { Headers, RequestMethod, StartNetworkLoggingOptions } from "./types";
type XHR = {
    _index: number;
    responseHeaders?: Headers;
};
interface LoggerProps {
    requests: NetworkRequestInfo[];
    xhrIdMap: {
        [key: number]: number;
    };
    maxRequests: number;
    ignoredHosts: Set<string> | undefined;
    ignoredUrls: Set<string> | undefined;
    ignoredPatterns: RegExp[] | undefined;
    enabled: any;
    paused: any;
}
export default class Logger implements LoggerProps {
    requests: NetworkRequestInfo[];
    xhrIdMap: any;
    maxRequests: number;
    ignoredHosts: Set<string> | undefined;
    ignoredUrls: Set<string> | undefined;
    ignoredPatterns: RegExp[] | undefined;
    enabled: boolean;
    paused: boolean;
    constructor();
    callback: (_: any[]) => void;
    setCallback: (callback: any) => void;
    getRequest: (xhrIndex?: number) => NetworkRequestInfo | undefined;
    updateRequest: (index: number, update: Partial<NetworkRequestInfo>, isOk?: any) => void;
    openCallback: (method: RequestMethod, url: string, xhr: XHR) => void;
    requestHeadersCallback: (header: string, value: string, xhr: XHR) => void;
    headerReceivedCallback: (responseContentType: string, responseSize: number, _: Headers, xhr: XHR) => void;
    sendCallback: (data: string, xhr: XHR) => void;
    responseCallback: (status: number, timeout: number, response: string, responseURL: string, responseType: string, xhr: XHR) => void;
    enableXHRInterception: (options?: StartNetworkLoggingOptions) => void;
    getRequests: () => NetworkRequestInfo[];
    clearRequests: () => void;
}
export {};
//# sourceMappingURL=Logger.d.ts.map