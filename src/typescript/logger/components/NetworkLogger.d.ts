import React from "react";
import { Theme, ThemeName } from "../theme";
import { DeepPartial } from "../types";
interface Props {
    theme?: ThemeName | DeepPartial<Theme>;
    sort?: "asc" | "desc";
    onBack?: any;
}
declare const NetworkLogger: React.FC<Props>;
export default NetworkLogger;
//# sourceMappingURL=NetworkLogger.d.ts.map