declare const ColorsLog: {
    Gray_30: string;
    Gray_57: string;
    Gray_9c: string;
    Gray_c4: string;
    Gray_da: string;
    Gray_e8: string;
    Gray_f4: string;
    White: string;
    Pri_fff: string;
    Pri_ffe: string;
    Pri_feb: string;
    Pri_ffa: string;
    Pri_fa7: string;
    Pri_fa4: string;
    font_30: string;
    font_57: string;
    font_74: string;
    font_9c: string;
    font_c4: string;
    font_da: string;
    bg_fff: string;
    bg_ffe: string;
    bg_575: string;
    bg_c4c: string;
    bg_dad: string;
    bg_f4f: string;
    stroke_fe: string;
    stroke_fa: string;
    stroke_57: string;
    stroke_9c: string;
    stroke_c4: string;
    stroke_da: string;
    stroke_e8: string;
    stroke_f4: string;
    pont_26: string;
    badge_e9: string;
    badge_e2: string;
    badge_28: string;
    Black: string;
};
export default ColorsLog;
//# sourceMappingURL=ColorsLog.d.ts.map