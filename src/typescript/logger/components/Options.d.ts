import React from 'react';
interface Props {
    options: {
        text: string;
        onPress: () => Promise<void> | void;
    }[];
}
declare const Options: React.FC<Props>;
export default Options;
//# sourceMappingURL=Options.d.ts.map