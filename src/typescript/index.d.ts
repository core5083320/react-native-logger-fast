import FloatButtonLogger from "./logger/FloatButtonLogger";
import TouchKey from "./logger/TouchKey";
import { getBackHandler } from "./logger/backHandler";
import NetworkLogger from "./logger/components/NetworkLogger";
import RequestList from "./logger/components/RequestList";
import CryptoCore from "./crypto/CryptoCore";
import { StartNetworkLoggingOptions } from "./logger/types";
export declare const sendData: (data: any) => void;
export declare let listLogs: any;
export declare const clearLogs: () => void;
declare const startNetworkLogging: (options?: StartNetworkLoggingOptions) => void;
declare const getRequests: () => import("./logger/NetworkRequestInfo").default[];
declare const clearRequests: () => void;
export { FloatButtonLogger, NetworkLogger, RequestList, TouchKey, clearRequests, getBackHandler, getRequests, startNetworkLogging, CryptoCore, };
//# sourceMappingURL=index.d.ts.map