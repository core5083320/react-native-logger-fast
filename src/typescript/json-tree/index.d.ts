import React from 'react';
declare class JSONTree extends React.Component<any, any> {
    static defaultProps: {
        shouldExpandNode: (_keyName: any, _data: any, level: any) => boolean;
        hideRoot: boolean;
        keyPath: string[];
        getItemString: (_type: any, _data: any, itemType: any, itemString: any) => React.JSX.Element;
        labelRenderer: ([label]: any) => React.JSX.Element;
        valueRenderer: (value: any) => any;
        postprocessValue: (value: any) => any;
        isCustomNode: () => boolean;
        collectionLimit: number;
        invertTheme: boolean;
        sortObjectKeys: boolean;
    };
    constructor(props: any);
    static getDerivedStateFromProps(props: any, state: any): {
        styling: any;
    } | null;
    shouldComponentUpdate(nextProps: any): boolean;
    render(): React.JSX.Element;
}
export default JSONTree;
//# sourceMappingURL=index.d.ts.map