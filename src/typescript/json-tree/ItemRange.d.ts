import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
export default class ItemRange extends PureComponent {
    static propTypes: {
        styling: PropTypes.Validator<(...args: any[]) => any>;
        from: PropTypes.Validator<number>;
        to: PropTypes.Validator<number>;
        renderChildNodes: PropTypes.Validator<(...args: any[]) => any>;
        nodeType: PropTypes.Validator<string>;
    };
    state: {
        expanded: boolean;
    };
    handlePress: () => void;
    render(): React.JSX.Element;
}
//# sourceMappingURL=ItemRange.d.ts.map