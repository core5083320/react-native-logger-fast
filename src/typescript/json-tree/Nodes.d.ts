import PropTypes from "prop-types";
import React from "react";
export declare const JSONObjectNode: {
    ({ data, ...props }: any): React.JSX.Element;
    propTypes: {
        data: PropTypes.Requireable<object>;
    };
};
export declare const JSONNode: {
    ({ getItemString, isCustomNode, keyPath, labelRenderer, styling, value, valueRenderer, ...rest }: any): React.JSX.Element | null;
    propTypes: {
        getItemString: PropTypes.Validator<(...args: any[]) => any>;
        isCustomNode: PropTypes.Validator<(...args: any[]) => any>;
        keyPath: PropTypes.Validator<(NonNullable<string | number | null | undefined> | null | undefined)[]>;
        labelRenderer: PropTypes.Validator<(...args: any[]) => any>;
        styling: PropTypes.Validator<(...args: any[]) => any>;
        value: PropTypes.Requireable<any>;
        valueRenderer: PropTypes.Validator<(...args: any[]) => any>;
    };
};
export declare class JSONNestedNode extends React.PureComponent {
    static propTypes: {
        createItemString: PropTypes.Validator<(...args: any[]) => any>;
        collectionLimit: PropTypes.Requireable<number>;
        data: PropTypes.Requireable<any>;
        expandable: PropTypes.Requireable<boolean>;
        getItemString: PropTypes.Validator<(...args: any[]) => any>;
        hideRoot: PropTypes.Validator<boolean>;
        isCircular: PropTypes.Requireable<boolean>;
        keyPath: PropTypes.Validator<(NonNullable<string | number | null | undefined> | null | undefined)[]>;
        labelRenderer: PropTypes.Validator<(...args: any[]) => any>;
        level: PropTypes.Validator<number>;
        nodeType: PropTypes.Validator<string>;
        nodeTypeIndicator: PropTypes.Requireable<any>;
        shouldExpandNode: PropTypes.Requireable<(...args: any[]) => any>;
        sortObjectKeys: PropTypes.Requireable<NonNullable<boolean | ((...args: any[]) => any) | null | undefined>>;
        styling: PropTypes.Validator<(...args: any[]) => any>;
    };
    static defaultProps: any;
    constructor(props: any);
    handlePress: () => void;
    render(): React.JSX.Element;
}
export declare function JSONIterableNode({ ...props }: {
    [x: string]: any;
}): React.JSX.Element;
export declare const JSONArrayNode: {
    ({ data, ...props }: any): React.JSX.Element;
    propTypes: {
        data: PropTypes.Requireable<any[]>;
    };
};
//# sourceMappingURL=Nodes.d.ts.map